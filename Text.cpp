// Name: Gunhoo Yoon
// Seneca Student ID: 101 466 175
// Seneca email: gyoon1@myseneca.ca
// Date of completion: 2018-9-10
//
// I confirm that the content of this file is created by me,
// with the exception of the parts provided to me by my professor.

// Sanity checks
//	default constructor initialize to valid states (m_size = 0, m_text = nullptr) o
//	passing not existing file name default initializes o
//	copy constructor deep copies and copies all members o
//  resistance to copy constructing from bad object o
//  copy operator is self-assigment free o
//  resistance to copy assigning bad object o
//  move constructor moves all members o
//	resistance to move constructing from bad object o
//  move assignment is self-assignment free o
//  resistance to move constructing from bad object o


#include <string>
#include <fstream>
#include <algorithm>
#include <iterator>

#include "./Text.h"

namespace w2 {
	Text::Text(const char* filename) {
		std::ifstream ifs{ filename };

		if (!ifs.is_open()) return;

		// std::count counts the target reference value from the start to the end of an iterator.
		// std::istreambuf_iterator is an iterator for objects derived from std::basic_istream.
		// https://en.cppreference.com/w/cpp/iterator/istreambuf_iterator
		size_t line_count = std::count(
			std::istreambuf_iterator<char>(ifs),
			std::istreambuf_iterator<char>(), // end-of-stream iterator
			'\n'
		);

		// rewind ifstream
		ifs.clear();
		ifs.seekg(0);

		// extra line to account for the last line without trailing newline.
		const size_t size = line_count + 1;
		m_text = new std::string[size];
		
		for (size_t i = 0; i < size; ++i) {
			std::getline(ifs, m_text[i]);
		}

		m_size = size;
	}

	Text::Text(const std::string& filename)
		: Text(filename.c_str())
	{

	}

	Text::Text(const Text& src) {
		if (src.size() < 1) return;

		m_text = new std::string[src.size()];

		for (size_t i = 0; i < src.size(); ++i) {
			m_text[i] = src.m_text[i];
		}
		m_size = src.size();
	}

	void swap(Text& a, Text& b) noexcept {
		using std::swap;

		swap(a.m_size, b.m_size);
		swap(a.m_text, b.m_text);
	}

	Text::Text(Text&& src) noexcept {
		swap(*this, src);
	}

	Text& Text::operator=(const Text& rhs) {
		Text tmp{ rhs };
		swap(*this, tmp);

		return *this;
	}

	Text& Text::operator=(Text&& rhs) noexcept {
		swap(*this, rhs);

		return *this;
	}

	Text::~Text() {
		delete[] m_text;
	}

	size_t Text::size() const {
		return m_size;
	}
}