// Name: Gunhoo Yoon
// Seneca Student ID: 101 466 175
// Seneca email: gyoon1@myseneca.ca
// Date of completion: 2018-9-10
//
// I confirm that the content of this file is created by me,
// with the exception of the parts provided to me by my professor.

#include "./Text.h"
#include <iostream>
#include <utility>
#include <string>

using namespace w2;

int main() {
	Text empty;
	
	Text bad{ "missing-file" };

	Text bad_copy{ bad };

	bad_copy = bad_copy;

	Text bad_copy_assigned;
	bad_copy_assigned = bad;

	Text bad_move{ std::move(bad) };

	Text bad_move_assigned;
	bad_move_assigned = Text{ "missing-file" };

	bad_move_assigned = Text{ std::move(bad_move_assigned) };

	Text text{ "C:\\Users\\gunhoo\\source\\repos\\oop345w2\\gutenberg_shakespeare" };

	using namespace std::string_literals;
	Text text2{ "C:\\Users\\gunhoo\\source\\repos\\oop345w2\\gutenberg_shakespeare"s };

	Text good{ "C:\\Users\\gunhoo\\source\\repos\\oop345w2\\gutenberg_shakespeare" };

	Text good_copy{ good };

	good_copy = good_copy;

	Text good_copy_assigned;
	good_copy_assigned = good;

	Text good_move{ std::move(good) };

	Text good_move_assigned;
	good_move_assigned = Text{ "C:\\Users\\gunhoo\\source\\repos\\oop345w2\\gutenberg_shakespeare" };

	good_move_assigned = Text{ good_move_assigned };
}