// Name: Gunhoo Yoon
// Seneca Student ID: 101 466 175
// Seneca email: gyoon1@myseneca.ca
// Date of completion: 2018-9-10
//
// I confirm that the content of this file is created by me,
// with the exception of the parts provided to me by my professor.

- Working with dynamically allocated array of std::string.
- Correctly, implementing special member functions for a resource handling class.
- Copy and Swap idiom.
- Move and Swap idiom, the Rule of Four.
- Converting constructor.
- Using explicit constructor.
- Using delegating constructor.
- Reading lines from a file.
- Counting lines from a file using std::count.
- istreambuf_iterator can be used with any object derived from basic_istream.