// Name: Gunhoo Yoon
// Seneca Student ID: 101 466 175
// Seneca email: gyoon1@myseneca.ca
// Date of completion: 2018-9-10
//
// I confirm that the content of this file is created by me,
// with the exception of the parts provided to me by my professor.

#ifndef W2_TEXT_H
#define W2_TEXT_H

#include <string>

namespace w2 {

	class Text {
	public:
		Text() = default;
		explicit Text(const char* filename);
		explicit Text(const std::string& filename);

		Text(const Text&);
		Text& operator=(const Text&);
		Text(Text&&) noexcept;
		Text& operator=(Text&&) noexcept;

		~Text();

		size_t size() const;
	
		friend void swap(Text&, Text&) noexcept;

	private:
		std::string* m_text = nullptr;
		size_t m_size = 0;
	};

}
#endif